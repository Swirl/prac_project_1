#include "include/graph_library.h"

bool check(bool *cpu_parallel_result, bool *cpu_sequential_result, int vertices_count)
{
    bool result = true;
    for(int i = 0; i < vertices_count; i++)
    {
        if(cpu_parallel_result[i] != cpu_sequential_result[i])
        {
            result = false;
        }
    }
    return result;
}

#include <fstream>
using std::fstream;

int main(int argc, char **argv)
{
    try
    {
        int threads = atoi(argv[1]);
        const char* graphType = argv[2];

        for (int i = 18; i <= 22; ++i)
        {

            double MTEPS;

            cout << graphType << endl;

            const char RMAT_GRAPH_TYPE[] = "RMAT";

            //int threads = omp_get_max_threads();

            // load graph
            Graph<int, float> graph(0, 0, EDGES_LIST, true);
            Graph<int, float> graph2(0, 0, EDGES_LIST, true);

            // generate graph

            if(strcmp(graphType, RMAT_GRAPH_TYPE) == 0)
            {
                cout << "Generating RMAT graph..." << endl;
                GraphGenerationAPI<int, float>::R_MAT_parallel(graph, pow(2.0, scale), 16, 45, 20, 20, 15, 24, true);
            }
            else
            {
                cout << "Generating SSCA2 graph..." << endl;
                GraphGenerationAPI<int, float>::SSCA2(graph, pow(2.0, scale), 5, true);
            }
            graph2 = graph;
            cout << "done!" << endl << endl;

            int vertices_count = graph.get_vertices_count();

            BFS<int, float> operation;
            operation.set_omp_threads(threads);

            graph.convert_to_edges_list();

            bool *cpu_parallel_result = new bool[vertices_count];
            bool *cpu_sequential_result = new bool[vertices_count];


            unsigned int seed = int(time(NULL));
            int source_vertex = rand_r(&seed) % vertices_count;
            cout << "source vertex " << source_vertex << endl;

            // parallel
            double start_time = omp_get_wtime();
            operation.cpu_parallel_bfs(graph, cpu_parallel_result, source_vertex);
            double time = omp_get_wtime() - start_time;
            cout << "computation time " << time << endl;

            MTEPS = graph.get_edges_count()/((time)*1e6);
            cout << "MTEPS " << MTEPS << endl;
            cout << "edge count: " << graph.get_edges_count() << endl;

            // sequential
            start_time = omp_get_wtime();
            operation.cpu_sequential_bfs(graph2, cpu_sequential_result, source_vertex);
            time = omp_get_wtime() - start_time;

            double seqMTEPS = graph2.get_edges_count()/((time)*1e6);

            // check result
            bool correctResultFlag = check(cpu_parallel_result, cpu_sequential_result, vertices_count);

            // txt output
            FILE* results;
            FILE* checkResults;
            if(strcmp(graphType, RMAT_GRAPH_TYPE) == 0)
            {
                results = fopen("results_RMAT.txt","a");
                checkResults = fopen("check_results_RMAT.txt","a");
            }
            else
            {
                results = fopen("results_SSCA2.txt","a");
                checkResults = fopen("check_results_SSCA2.txt","a");
            }
            fprintf(results, "%d %d %f", threads, scale, MTEPS);
            fprintf(results, "\r\n");
            fprintf(checkResults, "%d %d %f %f %d", threads, scale, MTEPS, seqMTEPS, correctResultFlag ? 1 : 0);
            fprintf(checkResults, "\r\n");
            fclose(results);
            fclose(checkResults);
            delete []cpu_parallel_result;
            delete []cpu_sequential_result;
        }
    }
    catch (const char *error)
    {
        cout << error << endl;
        getchar();
        return 1;
    }
    catch (...)
    {
        cout << "unknown error" << endl;
    }


    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
